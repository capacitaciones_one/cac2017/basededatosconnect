package comision1917conexionabd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Comision1917ConexionABD {

    public static void main(String[] args) {
        Connection con;
        
        try {
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost/comision1917",
                     "docente", "docente");

            ResultSet consulta;
            Statement stmt = con.createStatement();
            consulta = stmt.executeQuery(
                    "SELECT * FROM estudiantes");
            
            while(consulta.next()){
                System.out.print(consulta.getString("nombre") + ",");
                System.out.println(consulta.getString("apellido"));   
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }

}
